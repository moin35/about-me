# About Me

Recent Projects: 
1. www.lecturepublications.org (Laravel)
2. www.boibazar.com (Laravel)
3. boibazar.com/blog (wordpress)
4. www.missworldbd.com (wordpress and laravel)
5. https://healthncosmetics.com
6. Omicon IDCR (c#, dot net, Oracle)
7. Omicon ERP (c#, dot net, Oracle)
8. http://www.haatbangla.com/ (Laravel + Angular 5)
9. http://www.quiz.boibazar.com 


My Some Projects URL and Information given below:
1. Android Vehicle Tracking App
Description: Android Vehicle Tracking App. Using This app selected user can know there vehicle current position, photo and other information.
Technology Used: Java, Android, Volley Library, Google Map, api.
Project Duration: 45 day

2. Android FTP Client App
Description: Android FTP Client App. This is an simple local FTP client android application. Using this app selected client download there previous captured video and see.
Technology Used: Java, Android, Volley Library, apache ftp client library.
Project Duration: 20 day

3. http://www.accfintax-bd.com/(laravel)

4. http://airocitybd.com/(laravel)

5. http://tobaccofreewe.com/(Laravel)
Description: TobaccoFreeWe is an independent online platform that creates awareness about the health harms of tobacco use and tobacco control policies in Bangladesh. The page will sensitize the respective policy-makers to create the support base for policies.

6. SMS Sending Application (Laravel)

7. Stock Management (Laravel)

8. Client Management (Laravel)

10. https://github.com/moin35/Google-Map-Drawing-Tools
Description: Google Map Custom Marker Drawing.

11. http://akashtech.com/(laravel)
Description: Akash CMS Webapplication

12. http://www.ecart-bd.com/
Description: E-commerce web aplication

13. http://arraybilling.arraytechbd.com/auth/login (laravel)
Description: Accounting Software

14. http://www.amarbrand.com/
Description: This E commerce website has been built for the amarbrand.com, This e-portal like Shopping cart including admin control for viewing the order information product information, New entry of product, edit the specified product, delete the product etc. User login password, new admin registration, data fetching from database, feedback form etc.
Technology Used: PHP / MySQL, HTML, CSS, Java Script, Laravel.
Project Duration : 1-4 Months

15. https://liviaminc.com/
Description: Simple career management web site.
Technology Used: PHP / MySQL, HTML, CSS, Java Script, Laravel.
Project Duration : 1-3 Months

16. http://namcsch.com/ (School management system).
Description: Student management system (SMS) is software to manage all day to day operations for a school.
Technology Used: PHP / MySQL, HTML, CSS, Java Script, Laravel.
Project Duration: 1-6 Months

17. http://www.tokaipower.com/
Description: Simple Dynamic Web Application , using this application they can manage there company information.
Technology Used: PHP / MySQL, HTML, CSS, Java Script, Laravel.
Project Duration: 15 day
Team: 1 Member


18. https://github.com/moin35/akashnocproject
Description: Simple task management project .
Technology Used: PHP / MySQL, HTML, CSS, Java Script, Laravel.
Project Duration: 1 Months

19. https://github.com/moin35/multilanguage
Description: Multiple Languages in one application. 
